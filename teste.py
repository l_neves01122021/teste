'''
from sqlalchemy import create_engine
from sqlalchemy.sql import text

# Criando a conexão com o banco de dados:
engine = create_engine('postgresql://scott:data.merqueo.com:10100/merqueo_dwh')

# Criando tabela:
def consultar():
    with engine.connect() as con:
        consulta = text ("""
        select
            concat(u.first_name, ' ', u.last_name) user_name
	        , op.order_id
	        , op.reference
	        , op.product_name
	        , op.quantity
	        , op.price 
	        , op.quantity * op.price as total 
        from 
        	order_products op
        inner join orders o on o.id = op.order_id
        inner join users u on u.id = o.user_id 
        where op.order_id = '5515914'
        order by op.reference""")
        rs = con.execute(consulta)
        rs.fetchone()
        return dict(rs)

consultar()
'''

print("hello world")